<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Invalid Login tes</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ffc2b274-0cf4-479d-82ab-2dfda69c344c</testSuiteGuid>
   <testCaseLink>
      <guid>ef0440f3-3656-4b1f-9d25-0b862f130f04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SC1-login/TC2 - Invalid Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>461cd999-ec75-45dc-b30b-fb8fb7c147c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/InvalidLogin</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>461cd999-ec75-45dc-b30b-fb8fb7c147c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>760e8932-6ba1-4d1b-8ef1-e0e3ea909097</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>461cd999-ec75-45dc-b30b-fb8fb7c147c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>5a58a55e-6955-4c4b-9594-20351e7f06c2</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
