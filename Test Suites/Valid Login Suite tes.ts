<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Valid Login Suite tes</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>bab97391-4c62-4b0b-9054-4aacb95db543</testSuiteGuid>
   <testCaseLink>
      <guid>3b3eae10-47d2-4da7-afd9-998d4f1a99e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SC1-login/TC1-Valid Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>482ccf79-2038-4d02-9b40-d0ca55d970e9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ValidLogin</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>482ccf79-2038-4d02-9b40-d0ca55d970e9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>cd4d6953-883d-423f-81d7-4338ab2b815b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>482ccf79-2038-4d02-9b40-d0ca55d970e9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>863501ab-039f-43bb-8f0f-7345fbf56936</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
